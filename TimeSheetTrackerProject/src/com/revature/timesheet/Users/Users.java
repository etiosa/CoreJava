package com.revature.timesheet.Users;

import java.time.LocalDate;

public class Users {
private String firstName;
private String lastName;
private String userName;
private String passWord;
private Double hours;
private LocalDate date;

public Users(String userName, String passWord, double hours, LocalDate date)
{
	this.userName=userName;
	this.passWord=passWord;
	this.hours=hours;
	this.date=date;
}
public Users(String firstName, String lastName, String userName, String passWord)
{
	this.firstName=firstName;
	this.lastName=lastName;
	this.userName=userName;
	this.passWord=passWord;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassWord() {
	return passWord;
}
public void setPassWord(String passWord) {
	this.passWord = passWord;
}
public Double getHours() {
	return hours;
}
public void setHours(Double hours) {
	this.hours = hours;
}
public LocalDate getDate() {
	return date;
}
public void setDate(LocalDate date) {
	this.date = date;
}


	
}
