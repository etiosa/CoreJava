package com.homwork.q16;

public class Question16 {

	public static void main(String[] args) {
		int counter = 0;

		for(String contents : args)
		{
			counter += contents.length();
		}

		if(args.length > 1)
		{
			counter+= args.length-1;
		}

		System.out.println("The amount of characters is: " + counter);

	}

}


