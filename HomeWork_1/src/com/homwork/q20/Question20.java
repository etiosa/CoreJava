package com.homwork.q20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.StringTokenizer;

public class Question20 {

	public static void main(String[] args) {

		try{
			File file = new File("Data.txt");	
		FileReader readefile = new FileReader(file);
		BufferedReader br = new BufferedReader (readefile); 	

		String line;
		StringTokenizer str = null;

		while((line = br.readLine()) != null)
		{
			str = new StringTokenizer(line, ":");
			String[] parse = {"Name: ", "Age: ", "State: "};

			int i = 0;                     

			System.out.println(parse[i] + str.nextToken() + " " + str.nextToken());

			while(str.hasMoreTokens()) 
			{
				i++;
				System.out.println(parse[i] + str.nextToken());	

				while(str.hasMoreTokens()) 
				{
					i++;
					System.out.println(parse[i] + str.nextToken() + " State");						
					}
				}

				System.out.println();
			}

			br.close();
			readefile.close();

		}
		catch (FileNotFoundException fnf){
			fnf.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
