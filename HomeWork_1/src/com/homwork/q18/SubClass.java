package com.homwork.q18;

public class SubClass  extends TheSuperClass{

	@Override
	public boolean isUpper(String cont) {
		for (int i=0; i<cont.length(); i++)
		{
			
			if (Character.isUpperCase(cont.charAt(i)))
			{
				return true;
			}
		}

		return false;
	}


	@Override
	public String convertUpper(String cont) {
		// TODO Auto-generated method stub
		return cont.toUpperCase();
	}

	@Override
	public int addTen(String cont) {
		return Integer.parseInt(cont) + 10;
	}

}
