package com.homwork.q18;

public abstract class TheSuperClass {
	public abstract boolean isUpper(String cont);
	public abstract String convertUpper(String cont);
	public abstract int addTen(String cont);

}
