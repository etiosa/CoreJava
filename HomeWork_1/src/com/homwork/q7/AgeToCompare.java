package com.homwork.q7;

import java.util.Comparator;

public class AgeToCompare implements Comparator<Employee>{

	@Override
	public int compare(Employee o1, Employee o2) {

		if( o1.getAge()==o2.getAge())
		{
			return 0;
			
		}
		else if( o1.getAge()>o2.getAge())
		{
			return 1;
		}
		
		return -1;
	}

}
