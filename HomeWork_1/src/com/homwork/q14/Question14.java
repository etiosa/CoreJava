package com.homwork.q14;

import java.time.LocalDate;
import java.util.Scanner;

public class Question14 {


	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("SquareRoot, Calendar, StringArray");

		String pick = scanner.next();
		switch(pick){ 

		case "SquareRoot":

			System.out.print("Enter a number and find a Square Root: ");

			Double num = scanner.nextDouble();

			double root = Math.sqrt(num);

			System.out.println("Square Root is: " + root);

			break;

		case "Calendar":

			System.out.println("Today's date: " + LocalDate.now());

			break;

		case "StringArray":

			String str = "I am learning Core Java";

			String[] StrA = str.split(" ");

			for (String myStr: StrA){
				System.out.println(myStr);
			}
			break;
		default:
			System.out.println("Method unknown");

			scanner.close();
		}
	}

}
