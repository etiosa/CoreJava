package com.homwork.q11;

import com.homwork.q11_1.TestPackageVariable;

public class Question11 {

	public static void main(String[] args) {
		TestPackageVariable mypack = new TestPackageVariable ();

		System.out.println("First float variable: " + mypack.number_1);
		System.out.println("Second float variable: " + mypack.number_2);
	}

}
