package com.homwork.q15;

public class ArthmeticImplementation  implements ArthmeticInterface{

	@Override
	public int Add(int number_1, int number_2) {
		// TODO Auto-generated method stub
		return number_1+number_2;
	}

	@Override
	public int Subtract(int number_1, int number_2) {
		// TODO Auto-generated method stub
	
		return number_1-number_2;
	}

	@Override
	public int Multiply(int number_1, int number_2) {
 
		return number_1*number_2;
		
	}

	@Override
	public double Divide(double number_1, double number_2) {
		// TODO Auto-generated method stub
		if( number_2==0)
		{
			return -1;
		}
		else {
		return number_1/number_2;
		}
		
	}

}
