package com.homwork.q15;

public interface ArthmeticInterface {
	public int Add(int number_1, int number_2);
	public int Subtract(int number_1, int number_2);
	public int Multiply(int number_1, int number_2);
	public double Divide(double number_1, double number_2);
}
