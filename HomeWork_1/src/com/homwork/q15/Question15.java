package com.homwork.q15;

public class Question15 {

	public static void main(String[] args) {

		ArthmeticInterface claculator = new ArthmeticImplementation();
		int number_1 = 6;
		int number_2 = 5;
		System.out.println("Add operation "+claculator.Add(number_1, number_2));

		System.out.println("Divide operation "+claculator.Divide(number_1, number_2));

		System.out.println("Subtract operation "+claculator.Subtract(number_1, number_2));

		System.out.println("Multiply operation "+claculator.Multiply(number_1, number_2));
	}

}
