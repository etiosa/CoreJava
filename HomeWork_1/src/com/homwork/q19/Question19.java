package com.homwork.q19;

import java.util.ArrayList;

public class Question19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> numberList = new ArrayList<Integer>();
		int even = 0;
		int odd = 0;

		System.out.println( "Array List: ");
		for (int i = 1; i <= 10; i++)
		{
			numberList.add(i);
			System.out.println(numberList.get(i - 1));

			if (i%2 == 0)
			{
				even = even + i;
			}
			else{
				odd = odd + i;
			}

		}

		System.out.println("\nSum of Evens: " + even);

		System.out.println("Sum of Odds: " + odd);

		System.out.println("\nNon Prime Numbers ");
		for (int i = 1; i <= numberList.size(); i++)
		{
			if(numberList.get(i)%i == 0){
				numberList.remove(i-1);
			}
			System.out.println(numberList.get(i - 1));
		}
	}

}


